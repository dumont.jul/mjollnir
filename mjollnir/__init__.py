#!/usr/bin/env python3
# coding: utf-8

# Imports
from flask import Flask, request, jsonify
from functools import wraps
import configparser
import re
import os

# Current version
__version__ = '1.0.0'

# Declare agent
app = Flask(__name__)

# Load configuration
config = configparser.ConfigParser()
config.optionxform = str
config.read('/etc/mjollnir/agent.ini')


# Before request: Global security rules
@app.before_request
def before_request_check():

    # Ensure agent is in non-maintenance mode (exception: status endpoints)
    if os.path.exists(config.get('system_files', 'maintenance')) and not re.match(r'^\/v\d+\/status$', request.path):
        return jsonify({'error': 'Agent is in maintenance mode'}), 503

    # Ensure query comes from an authorized master server
    if request.remote_addr not in config.get('master', 'nodes').split(','):
        return jsonify({'error': 'You are not an authorized master node.'}), 403


# Custom decorator: API endpoint requires a valid token
def token_required(f):
    """
    Handle the token-based authentication
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):

        # API credentials
        api_token_ref = config.get('token', 'header')
        api_token_val = config.get('token', 'value')

        # Classic token validation
        if api_token_ref not in request.headers or request.headers[api_token_ref] != api_token_val:
            return jsonify({'error': 'You are not authorized to access to this endpoint!'}), 403

        # Return route
        return f(*args, **kwargs)

    return decorated_function


# Default error handler for unexisting endpoint
@app.errorhandler(404)
def api_endpoint_notfound(e):
    return jsonify({
        'error': 'That API endpoint does not exists'
    }), 501


# Perform a recursive replace
def recursive_replace(dictdata, **values):
    for k, v in dictdata.items():
        if isinstance(v, dict):
            dictdata[k] = recursive_replace(v, **values)
        else:
            dictdata[k] = v.format(**values)
    return dictdata


# Import master versions
from mjollnir import v1
