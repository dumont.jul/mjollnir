#!/usr/bin/env python3
# coding: utf-8

# Imports
from flask import request


# Payload management
def payload(keyname, **kwargs):

    # Load payload
    pl = request.get_json(force=True, silent=True) or {}

    # Check if keyname exists
    if keyname not in pl and kwargs.get('strict', True) is True:
        raise ValueError('Missing {} in payload!'.format(keyname))
    if keyname not in pl:
        return None
    keyvalue = pl[keyname]

    # Ensure the value is kind of expected type
    expected_type = kwargs.get('type', None)
    if expected_type is not None and not isinstance(keyvalue, expected_type):
        raise ValueError('{} is not an expected {}!'.format(keyname, expected_type))

    # Return value
    return keyvalue


# v1 modules
from mjollnir.v1 import health
from mjollnir.v1 import jobs
