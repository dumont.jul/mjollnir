#!/usr/bin/env python3
# coding: utf-8

# Imports
from flask import jsonify, __version__ as flaskvers
from mjollnir import app, config, token_required, __version__ as appvers
from docker import __version__ as dockerlibvers
import os
import docker
import socket
import glob
import hashlib


# Return node infos
@app.route('/v1/status')
@token_required
def v1_status():

    # Load docker setup
    cli = docker.from_env()
    docker_info = cli.info()

    # Load agent maintenance mode
    maintenance = False
    maintenance_file = config.get('system_files', 'maintenance')
    if os.path.exists(maintenance_file):
        fstat = os.stat(maintenance_file)
        maintenance_comment = open(maintenance_file, 'r').readline().strip() or None
        maintenance = {
            'set_at': fstat.st_ctime,
            'last_update': fstat.st_mtime,
            'comment': maintenance_comment
        }

    # List jobs
    jobs = {}
    jobs_dir = config.get('system_files', 'jobs')
    if jobs_dir:
        for job_file in glob.glob('{}/*.json'.format(jobs_dir)):
            job_name = os.path.basename(job_file)
            with open(job_file, 'r') as hdr:
                job_content = hdr.read()
                hdr.close()
            jobs[job_name] = hashlib.md5(job_content).hexdigest()

    # Return node status
    return jsonify({
        'node': {
            'version': appvers,
            'hostname': socket.getfqdn()
        },
        'pyenv': {
            'Flask': flaskvers,
            'Docker': dockerlibvers
        },
        'docker': {
            'version': docker_info['ServerVersion'],
            'containers': docker_info['Containers']
        },
        'maintenance': maintenance,
        'jobs': jobs
    })


# Clean node
@app.route('/v1/clean')
@token_required
def v1_clean():

    # Load docker setup
    cli = docker.from_env()

    # System prune
    try:
        cli.containers.prune()
        cli.networks.prune()
        cli.volumes.prune()
        cli.images.prune()
    except Exception as e:
        return jsonify({
            'error': 'Fail to clean node',
            'detail': str(e)
        })

    # Return node clean status
    return jsonify({
        'success': 'Node containers cleaned'
    })
