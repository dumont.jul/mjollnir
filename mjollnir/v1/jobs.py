#!/usr/bin/env python3
# coding: utf-8

# Imports
from flask import jsonify
from mjollnir import app, token_required, config, recursive_replace
from mjollnir.v1 import payload
import hashlib
import docker
import json
import shutil
import os
import re
import humanize


# Start a new Job
@app.route('/v1/job/<jobname>/<int:jobid>', methods=['POST'])
@token_required
def v1_job_start(jobname, jobid):

    # Check job name
    if not re.match(r'^\w+$', jobname):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400

    # Load job configuration
    jobs_dir = config.get('system_files', 'jobs')
    job_path = '{}/{}.json'.format(jobs_dir, jobname)
    if not os.path.exists(job_path):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400
    try:
        with open(job_path, 'r') as hdr:
            job = json.load(hdr)
            hdr.close()
    except Exception as e:
        return jsonify({
            'error': 'Could not load job definition',
            'detail': str(e)
        }), 500

    # Load default environment settings
    env_settings = {}
    for k, v in job['environment'].items():
        env_settings[k] = v

    # Get Git branch & environment settings
    branch = payload('branch', type=str)
    feature = branch.split('/')[-1].replace('_', '-')

    # Replacement variables
    env_vars = {
        'feature': feature,
        'branch': branch,
        'jobid': jobid,
        'md5_feature': hashlib.md5(feature.encode('utf-8')).hexdigest(),
        'md5_branch': hashlib.md5(branch.encode('utf-8')).hexdigest()
    }

    # Update environment variables
    env_settings = recursive_replace(env_settings, **env_vars)

    # Reference listening port
    port = job['root_port'] + jobid

    # Connect to Docker
    cli = docker.from_env()

    # Create network if not exists
    network_name = job['network'].format(**env_vars)
    try:
        if cli.networks.get(network_name):
            return jsonify({
                'error': 'Job ID already in use'
            }), 423
    except docker.errors.NotFound:
        pass
    try:
        cli.networks.create(network_name, driver='bridge')
    except Exception as e:
        return jsonify({
            'error': 'Could not create environment network',
            'detail': str(e)
        }), 500

    # Common container options
    container_options_common = {
        'auto_remove': False,       # To get dev logs
        'detach': True,
        'network': network_name,
        'environment': env_settings,
        'tty': True
    }

    # Job unique requirements
    already_exposed = None

    # Raise containers
    for container_name, container_settings in job['containers'].items():
        if 'image' not in container_settings:
            return jsonify({
                'error': 'Server configuration error',
                'detail': 'Missing image definition'
            }), 512
        if 'reference' in container_settings:
            reference = container_settings['reference']
        reference = '{}.{}.{}'.format(reference, jobid, feature)
        container_options = {
            'hostname': container_name,
            'name': reference.format(**env_vars)
        }
        if 'links' in container_settings:
            if not isinstance(container_settings['links'], list):
                return jsonify({
                    'error': 'Server configuration error',
                    'detail': 'Links are not a list for container {}'.format(container_name)
                }), 512
            for link in container_settings['links']:
                if ':' not in link:
                    return jsonify({
                        'error': 'Server configuration error',
                        'detail': 'Links are not a list for container {}'.format(container_name)
                    }), 512
            if 'links' not in container_options:
                container_options['links'] = {}
            container, alias = link.split(':')
            container = '{}.{}.{}'.format(container, jobid, feature)
            container_options['links'][container] = alias
        if 'entrypoint' in container_settings:
            container_options['entrypoint'] = container_settings['entrypoint']
        if 'expose' in container_settings:
            if already_exposed:
                return jsonify({
                    'error': 'Server configuration error',
                    'detail': '''
                        Could not expose application from multiple containers (exposing: {}, duplicate: {})
                        Ensure your application have a laod balancer if you need that feature.
                        '''.format(already_exposed, container_name).strip(),
                }), 512
            container_options['ports'] = {
                '{}/tcp'.format(container_settings['expose']): port
            }
            already_exposed = container_name
        try:
            cli.containers.run(container_settings['image'], **dict(container_options, **container_options_common))
        except Exception as e:
            return jsonify({
                'error': 'Could not start {} container'.format(container_name),
                'detail': str(e)
            }), 500

    # Check environment settings
    return jsonify({
        'success': 'Job started',
        'port': port
    })


# Stop a Job
@app.route('/v1/job/<jobname>/<int:jobid>', methods=['DELETE'])
@token_required
def v1_job_stop(jobname, jobid):

    # Check job name
    if not re.match(r'^\w+$', jobname):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400

    # Load job configuration
    jobs_dir = config.get('system_files', 'jobs')
    job_path = '{}/{}.json'.format(jobs_dir, jobname)
    if not os.path.exists(job_path):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400
    try:
        with open(job_path, 'r') as hdr:
            job = json.load(hdr)
            hdr.close()
    except Exception as e:
        return jsonify({
            'error': 'Could not load job definition',
            'detail': str(e)
        }), 500

    # Get Git branch & environment settings
    try:
        branch = payload('branch', type=str)
        feature = branch.split('/')[-1].replace('_', '-')
    except ValueError as e:
        return jsonify({
            'error': 'Invalid payload',
            'detail': str(e)
        }), 400

    # Replacement variables
    env_vars = {
        'feature': feature,
        'branch': branch,
        'jobid': jobid,
        'md5_feature': hashlib.md5(feature.encode('utf-8')).hexdigest(),
        'md5_branch': hashlib.md5(branch.encode('utf-8')).hexdigest()
    }

    # Connect to Docker
    cli = docker.from_env()

    # Get network details
    network_name = job['network'].format(**env_vars)
    try:
        network = cli.networks.get(network_name)
    except docker.errors.NotFound:
        return jsonify({
            'success': 'No such environment found'
        }), 200

    # Stop linked containers
    for container in network.containers:
        try:
            container.stop()
        except docker.errors.APIError as e:
            try:
                container.kill()
            except docker.errors.APIError as e:
                return jsonify({
                    'error': 'Could not stop container {}'.format(container.name),
                    'detail': str(e)
                }), 500
            except Exception as e:
                pass
        except Exception as e:
            pass

    # Remove network
    try:
        network.remove()
    except docker.errors.APIError as e:
        return jsonify({
            'error': 'Could not clean network',
            'detail': str(e)
        }), 500

    # System prune
    try:
        cli.containers.prune()
        cli.networks.prune()
        cli.volumes.prune()
        cli.images.prune()
    except Exception as e:
        pass

    # Debug
    return jsonify({
        'success': 'Job stopped'
    }), 200


# Check if Job can fit on the server
@app.route('/v1/job/<jobname>/fit', methods=['GET'])
@token_required
def v1_job_fit(jobname):

    # Check job name
    if not re.match(r'^\w+$', jobname):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400

    # Load job configuration
    jobs_dir = config.get('system_files', 'jobs')
    job_path = '{}/{}.json'.format(jobs_dir, jobname)
    if not os.path.exists(job_path):
        return jsonify({
            'error': 'Invalid job name!'
        }), 400
    try:
        with open(job_path, 'r') as hdr:
            job = json.load(hdr)
            hdr.close()
    except Exception as e:
        return jsonify({
            'error': 'Could not load job definition',
            'detail': str(e)
        }), 500

    # Connect to Docker
    cli = docker.from_env()

    # Get list of images used in the job
    images = []
    for container_name, container_settings in job['containers'].items():
        images.append(container_settings['image'])

    # Get sizes
    info = cli.df()
    sizes = {}
    computed_size = 0
    for container in info['Containers']:
        if 'Image' not in container or container['Image'] not in images:
            continue
        if 'SizeRootFs' in container:
            image = container['Image']
            container_size = container['SizeRootFs']
            if image not in sizes or sizes[image] < container_size:
                sizes[image] = container_size
    for image, size in sizes.items():
        computed_size += size

    # Compare with disk space
    docker_disk_partition = '/'
    if config.has_option('system_files', 'docker'):
        docker_disk_partition = config.get('system_files', 'docker')
    total, used, free = shutil.disk_usage(docker_disk_partition)
    delta = (computed_size*1.1) - free        # 10% of margin
    delta_human = humanize.naturalsize(abs(computed_size - free))
    if delta > 0:
        return jsonify({
            'error': 'Not enough free space',
            'detail': 'Missing {}'.format(delta_human)
        })

    # Default result
    return jsonify({
        'success': 'There are enough resources for a new job instance',
        'details': '{} available after a new job'.format(delta_human)
    })
