# Mjöllnir

This project is used to manage containers used to develop & test features.

This is not a production-side manager: it does not support High Availability, just a cluster of machines hosting a couple of containers with specific custom for each feature branch you can have.

[mjollnir.readthedocs.io](https://mjollnir.readthedocs.io/)
