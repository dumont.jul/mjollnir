# Mjöllnir Documentation

Welcome on the Mjöllnir project documentation website!

We aim to bring you a customizable environment to manage containers as jobs to develop/test your feature branch from your Git project.

This project IS NOT a production-class environment (with high availability cluster, redundancy, backups, failover, etc.). It is just a manager of container across multiple servers to serve multiple (un)stable versions of the same project (features branch).
