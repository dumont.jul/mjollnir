#!/usr/bin/env python3
# coding: utf-8

# Imports
from mjollnir import app

# Start application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6789, threaded=True, debug=True)
